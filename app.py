import asyncio
import websockets


uri = "ws://127.0.0.1:8000/ws/data_collector/trade/"


async def connect(uri):
    async with websockets.connect(uri) as websocket:
        while True:
            print(await websocket.recv())

asyncio.get_event_loop().run_until_complete(connect(uri))